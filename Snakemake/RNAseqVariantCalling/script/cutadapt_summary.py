#!/usr/bin/env python3
import os
import re
from collections import OrderedDict

def _build_lines_cutadapt(file):
    with open(file, "r") as log_file:
        line = log_file.readline()
        while line != "=== Summary ===\n":
            line = log_file.readline()
        line = log_file.readline().replace("\n","")
        occurences = 1
        while not line.startswith("==="):
            match = re.match(r"(.+):\s*(.+)", line)
            if match:
                head = match.group(1)
                if head == "  Read 1" or head == "  Read 2":
                    if occurences < 3:
                        occurences += 1
                    else:
                        head = head.replace("  Read", "  Read")
                value = match.group(2)
                value = re.sub("\(.+\)", "", value)
                value = value.replace(" bp", "").replace(",", "")
                if head not in res_lines:
                    res_lines[head] = []
                res_lines[head].append(value)
            line = log_file.readline().replace("\n", "")


sample_names = []
res_lines = OrderedDict()

if isinstance(snakemake.input.logs, list) :
    for cutadapt_log in snakemake.input.logs:
        _build_lines_cutadapt(cutadapt_log)
        sample_names.append(re.search(r"(.+)_trim.log", os.path.basename(cutadapt_log)).group(1))
else:
    _build_lines_cutadapt(snakemake.input.logs)
    sample_names.append(re.search(r"(.+)_trim.log", os.path.basename(snakemake.input.logs)).group(1))
        
with open(snakemake.output.out, "w") as summary_file:
    summary_file.write("\t" + "\t".join(sample_names) + "\n")
    for head, value in res_lines.items():
        summary_file.write(head + "\t" + "\t".join(value) + "\n")
