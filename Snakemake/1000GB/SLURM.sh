#!/bin/bash

##############################################################################
## launch workflow

module load system/R-3.4.3
module load system/Python-3.6.3

mkdir -p SLURM_logs

# Link into your working directory :
#   -Snakefile
#   -resources_SLURM.yaml
#   -script directory
#   -rules directory

# Create a input_data.tsv, following the data/input_data.tsv file example

# Copy config.yaml into your working directory, and manually update it

# Perform a dryrun before doing the analysis
snakemake -s Snakefile -p --jobs 200 --configfile config.yaml --dryrun

# Once the dryrun is ok, run the analysis
#~ snakemake -s Snakefile -p --jobs 200 \
           #~ --configfile config.yaml \
           #~ --cluster-config resources_SLURM.yaml \
           #~ --cluster "sbatch --cpus-per-task={cluster.cpu} --mem={cluster.mem} --error=SLURM_logs/%x.stderr --output=SLURM_logs/%x.stdout " --latency-wait 30
