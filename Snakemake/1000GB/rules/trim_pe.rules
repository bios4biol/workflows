# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.1"

"""
Rules for trimming paired reads of adapter and filtering out reads with mean qscore < 20 or length < 35bp using trimmomatic PE tool. Note that for modern Illumina adapter sequences, the fastq files are Phred+33 encoded. So before trimming, this rule will check the quality encoding of each fasqt file (Phred+33 or Phred+64) with fastq_dectect.pl script. if fastq files are Phred+64 encoded, then an option is added to trimmomatic 'TOPHRED33' to convert to Phred+33 encoding. 4 outputs are generated: forward paired, forward unpaired, reverse paired, reverse unpaired in fastq format.
"""

# guess PE fastq encoding (Phred33 or Phred64)
rule guess_encoding_pe:
    input:
        lambda wildcards: PE_reads[wildcards.unit]
    output:
        temp("results/trimmomatic/encoding/{unit}_encoding.txt")
    shell:
        "perl scripts/fastq_detect.pl {input[0]} 1000 > {output}"


rule trimmomatic_pe:
    input:
        reads=lambda wildcards: PE_reads[wildcards.unit],
        adapter=config["adapter_PE"],
        encoding="results/trimmomatic/encoding/{unit}_encoding.txt"
        
    output:
        temp("results/trimmomatic/PE/{unit}_1P.fastq.gz"),
        temp("results/trimmomatic/PE/{unit}_2P.fastq.gz"),
        temp("results/trimmomatic/PE/{unit}_1U.fastq.gz"),
        temp("results/trimmomatic/PE/{unit}_2U.fastq.gz"),
        protected("results/trimmomatic/PE/{unit}.summary")
        
    threads: config["trimmomatic_pe"]["cpu"]
    
    log:
        "results/trimmomatic/PE/logs/{unit}_trimming_PE.stderrs"

    params:
        mem= config["trimmomatic_pe"]["mem"]
    
    run:
        #add tophred33 option to trimmomatic if the file is phred+64 encoded.
        for i in open(input.encoding, "r"):
            if "#" not in i:
                if i.strip().split()[0] == "Phred+64" or i.strip().split()[0] == "Solexa+64":
                    add_option="TOPHRED33"
                elif i.strip().split()[0] == "Phred+33":
                    add_option=""
        
        shell("java -Xmx{params.mem} -jar {config[trimmomatic]} PE -threads {threads} -summary {output[4]} {input.reads} {output[0]} {output[2]} {output[1]} {output[3]} ILLUMINACLIP:{input.adapter}:2:30:3:1:true LEADING:20 TRAILING:20 SLIDINGWINDOW:3:15 AVGQUAL:20 MINLEN:35 "+add_option+" 2> {log}")

rule md5sum_trim_pe:
    input:
        "results/trimmomatic/PE/{unit}.summary"
    output:
        protected("results/md5sum_codes/{unit}.summary_PE.md5")
    log:
        "results/md5sum_codes/logs/{unit}.summary_PE.stderr"
    shell:
        "{config[md5sum]} {input} > {output} 2> {log}"
