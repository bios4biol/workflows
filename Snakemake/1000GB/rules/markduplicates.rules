# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.0"

"""
Rule for marking PCR and optical duplicates using Picard MarkDuplicates. Note that the option OPTICAL_DUPLICATE_PIXEL_DISTANCE equals 100 for data generated on non-arrayed flowcells or equals 2500 for arrayed flowcell data. The outputs are marked bam files per sample.
"""

rule markduplicates:
    """
    merged bam files are marked with markduplicates tool
    """
    input:
        "results/sample_merge/{sample}.sorted.bam"
        
    output:
        temp("results/marked_bams/{sample}_dedup.bai"),
        marked_bam=temp("results/marked_bams/{sample}_dedup.bam"),
        metrics=protected("results/marked_bams/{sample}_dedup.metrics")
        
    log: 
        stderr="results/marked_bams/logs/{sample}_duplicates.stderr",
        stdout="results/marked_bams/logs/{sample}_duplicates.stdout"
    
    params:
        mem=config["markduplicates"]["mem"]
    
    run:
        arrayed_flowcells=["GAIIx", "HiSeq1500", "HiSeq2000", "HiSeq2500"]
        non_arrayed_flowcells=["HiSeqX", "HiSeq3000", "HiSeq4000", "NovaSeq"]
        
        if Type_flowcell[wildcards.sample] in arrayed_flowcells:
            flowcell_param="OPTICAL_DUPLICATE_PIXEL_DISTANCE=100"
            
        elif Type_flowcell[wildcards.sample] in non_arrayed_flowcells:
            flowcell_param="OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500"
        
        mem=str(int(params.mem.replace("G","").replace("g",""))-4)+"G"
        
        shell("java -Xmx{mem} -jar {config[picard]} MarkDuplicates I={input} O={output.marked_bam} M={output.metrics} {flowcell_param} CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT 2> {log.stderr} > {log.stdout} ")
