# coding: utf-8

__author__ = "Maria BERNARD - Sigenae"
__version__ = "1.0.0"

"""
Index with tabix vcf files
Compute md5 code for final output files
"""

rule md5sum:
    input:
        "{file}"
    
    output:
        "{file}.md5"
        
    shell:
        "md5sum {input} > {output}"

rule bgzip:
    input:
        '{file}.vcf'
    output:
        '{file}.vcf.gz'
    shell:
        "bgzip {input}"
        
rule tabix:
    input:
        "{file}"
        
    output:
        temp("{file}.tbi")
        
    shell:
        "tabix -p vcf {input} "
        

rule bam_index:
    input:
        "{file}.bam"
        
    output:
        temp("{file}.bam.bai")
        
    shell:
        "samtools index {input} "
