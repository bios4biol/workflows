# coding: utf-8

__author__ = 'Maria BERNARD- Sigenae'
__version__ = '1.0.0'

'''
?????
'''
import sys
from pyfaidx import Fasta
genome = Fasta(config['fasta_ref'])
chrom_list = [ seq.name for seq in genome if seq.name.isdigit() ]

PREFIX = os.path.basename(config['vcf']).replace('.vcf.gz','')
    # GATK4
rule SelectVariants :
    input :
        vcf = config['vcf']
    output :
        vcf = temp("Results_ASE/vcfFiltration/" + PREFIX + "_SNP.vcf")
    params :
        mem = config["SelectVariants"]["mem"]
    shell:
        '''
        gatk --java-options "-Xmx{params.mem}" SelectVariants -V {input.vcf} -O {output.vcf} --select-type-to-include SNP
        '''

def get_SNP_vcf(wildcards):
    if config['SNP_filter']:
        return "Results_ASE/vcfFiltration/" + PREFIX + "_SNP.vcf"
    else:
        return config['vcf']

    # GATK3:
    # java -jar GenomeAnalysisTK.jar -T VariantFiltration -R {input.ref} -V {input.vcf} {params.filter_exp} -o {output.vcf}  -filterName "FS" -filter "FS > 30.0" -filterName "QD" -filter "QD < 2.0" --cluster-window-size 35 --cluster-size 3
    # GATK4
rule FS_QD_clust :
    input :
        vcf = get_SNP_vcf,
        ref = config['fasta_ref']
    output :
        vcf = temp("Results_ASE/vcfFiltration/" + PREFIX + "_FS_QD_clust_tagged.vcf"),
        idx = temp("Results_ASE/vcfFiltration/" + PREFIX + "_FS_QD_clust_tagged.vcf.idx")
    params :
        mem = config["FS_QD_clust"]["mem"]
    shell:
        '''
        gatk --java-options "-Xmx{params.mem}" VariantFiltration -R {input.ref} -V {input.vcf} -O {output.vcf} --filter-name FS --filter-expression "FS > 30.0" --filter-name QD --filter-expression "QD < 2.0" -window 35 -cluster 3
        '''

rule vcf_filtration :
    input :
        vcf = "Results_ASE/vcfFiltration/" + PREFIX + "_FS_QD_clust_tagged.vcf",
        idx = "Results_ASE/vcfFiltration/" + PREFIX + "_FS_QD_clust_tagged.vcf.idx",
        gtf = config['gtf_ref'],
        fasta = config['fasta_ref']
    output :
        vcf = temp("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_FsQdBiall.vcf") ,
        gt = temp("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_GT.tsv") ,
        dp = temp("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_DP.tsv" ),
        ad = temp("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_AD.tsv" ),
        summary = temp("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_VariantFiltration_summary.txt")
    params :
        chrom_selection = lambda wildcards : "--accepted-chrom " + wildcards.chrom if wildcards.chrom != "other" else "--excluded-chrom " + " ".join(chrom_list),
        cpu = config['vcf_filtration']['cpu']
    shell:
        '''
        variantFiltration.py --nb-cpus {params.cpu} --in-vcf {input.vcf} --in-gtf {input.gtf} --in-fasta {input.fasta} {params.chrom_selection} --out-vcf {output.vcf} --out-gt {output.gt} --out-dp {output.dp} --out-ad {output.ad} --summary {output.summary}
        '''

rule merge_filteredVariant :
    input :
        chr_vcf = expand("Results_ASE/vcfFiltration/by_chrom/{CHR}_" + PREFIX + "_FsQdBiall.vcf", CHR=chrom_list),
        other_vcf = "Results_ASE/vcfFiltration/by_chrom/other_" + PREFIX + "_FsQdBiall.vcf"
    output :
        vcf = "Results_ASE/vcfFiltration/" + PREFIX + "_FsQdBiall.vcf"
    shell:
        '''
        bcftools concat -o {output.vcf} -O v {input.chr_vcf} {input.other_vcf}
        '''
def get_table(wildcards):
    table_list = expand("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_"+wildcards.suffix + ".tsv", chrom=chrom_list )
    return table_list
    
rule merge_filtered_table:
    input :
        chr_tsv = get_table,
        other_tsv = "Results_ASE/vcfFiltration/by_chrom/other_" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_{suffix}.tsv"
    output :
        tsv = "Results_ASE/vcfFiltration/" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_{suffix}.tsv"
    wildcard_constraints:
        suffix="GT|AD|DP"
    shell:
        '''
        head -n 1 {input.other_tsv} > {output.tsv}
        cat {input.chr_tsv} {input.other_tsv}| grep -v "#" >> {output.tsv}
        '''

rule merge_filtered_summary:
    input :
        chr_tsv = expand("Results_ASE/vcfFiltration/by_chrom/{chrom}_" + PREFIX + "_VariantFiltration_summary.txt", chrom=chrom_list),
        other_tsv = "Results_ASE/vcfFiltration/by_chrom/other_" + PREFIX + "_VariantFiltration_summary.txt"
    output :
        summary = "Results_ASE/Summary/VariantFiltration_summary.txt"
    run : 
        dic_results = dict()
        in_list = input.chr_tsv
        in_list.append(input.other_tsv)
        # in_list = ['Results_ASE/vcfFiltration/by_chrom/1_test_VariantFiltration_summary.txt', 'Results_ASE/vcfFiltration/by_chrom/25_test_VariantFiltration_summary.txt', 'Results_ASE/vcfFiltration/by_chrom/26_test_VariantFiltration_summary.txt', 'Results_ASE/vcfFiltration/by_chrom/other_test_VariantFiltration_summary.txt']
        first=in_list.pop(0)
        FH_first = open(first,'rt')
        summary = ''
        first_key = ''
        percent_list = list()
        for line in FH_first:
            if ':' in line:
                category = line.split(':')[0]
                key = category.upper().strip().replace(' ','_')
                
                # store first key concidered as the total of variant
                if first_key == '':
                    first_key = key
                
                # store key where we want to compute percentage
                if '%)' in line:
                    percent_list.append(key)
                    
                value = int(line.split(':')[1].strip().split()[0])
                
                summary += category + ': ###' + key + '###\n'
                dic_results[key] = value
            
            elif "# Input vcf file is" in line:
                line = "# Input vcf file is " + "Results_ASE/vcfFiltration/" + PREFIX + "_FS_QD_clust_tagged.vcf\n"
                summary += line
            elif "those SNP are described in" in line:
                line = "\t\t\tthose SNP are described in " + "Results_ASE/vcfFiltration/" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_GT.tsv, Results_ASE/vcfFiltration/" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_DP.tsv, Results_ASE/vcfFiltration/" + PREFIX + "_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_AD.tsv\n"
                summary += line
            else:
                summary += line
        FH_first.close()

        for f in in_list:
            FH_in = open(f,'rt')
            for line in FH_in:
                if ':' in line:
                    category = line.strip().split(':')[0]
                    key = category.upper().strip().replace(' ','_')
                    value = int(line.strip().split(':')[1].strip().split()[0])
                    dic_results[key] += value
            FH_in.close()


        for k in dic_results:
            if k in percent_list:
                percent = round(dic_results[k]*100/dic_results[first_key],2)
                summary = summary.replace('###' + k + '###' , str(dic_results[k]) + ' (' + str(percent) + '%)')
            else:
                summary = summary.replace('###' + k + '###' , str(dic_results[k]))
                
        FH_out = open(output.summary,'wt')
        FH_out.write(summary)
        FH_out.close()
