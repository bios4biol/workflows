#!/bin/bash
##############################################################################
## launch workflow

# if used on Genologin cluster (INRA Toulouse )
# 	module load system/Python-3.6.3

WORKFLOW_DIR=.

mkdir -p logs

# This is an example of the snakemake command line to launch a dryrun
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dryrun

# This is an example of the snakemake command line to launch on a SLURM cluster
	# {cluster.cpu}, {cluster.mem} and {cluster.partition} will be replace by value defined in the resources_SLURM.yaml file
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example \
           --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem-per-cpu={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30
