#!/usr/bin/env python3
import os
import re
from collections import OrderedDict

def _build_lines_star(file, res_lines):
    key_value_line = r"\s*(.+)\s\|\t(.+)\n"
    with open(file, "r") as log_file:
        lines = log_file.readlines()
        for i in range(5, len(lines)):  # Remove header
            line = lines[i]
            match = re.match(key_value_line, line)
            if match:
                name = match.group(1)
                value = match.group(2).replace("%","")
                if name not in res_lines:
                    res_lines[name] = []
                res_lines[name].append(value)
    return res_lines

def _build_star_summary(star_summary_file, logs_files):
    with open(star_summary_file, "w") as sy_file:
        res_lines = OrderedDict()
        for log in logs_files:
            res_lines = _build_lines_star(log, res_lines)
            name_sample = re.search(r"(.+)_Log\.final\.out", os.path.basename(log)).group(1)
            sy_file.write("\t" + name_sample)
        sy_file.write("\n")
        for name_line, values_line in res_lines.items():
            sy_file.write(name_line)
            if values_line is not None:
                sy_file.write("\t" + "\t".join(values_line))
            sy_file.write("\n")
        sy_file.write("\n")


#Get outputs:
star_logs = snakemake.input.logs
starmap_summary = snakemake.output.out

# Build summary for STAR mapping:
_build_star_summary(starmap_summary, star_logs)
