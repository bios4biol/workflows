# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rules to "hard" filtered SNP and INDEL following the GATK recommandations
"""

rule VariantFiltration:
    input : 
        vcf = "results/split_SNP_INDEL/common_{var}.vcf.gz",
        ref = config["reference_genome"]
    output :
        vcf = temp("results/hard_filtering/common_{var}_tagged.vcf.gz")
    log :
        "results/hard_filtering/logs/common_{var}_variantFiltration.log"
    params:
        mem= config["VariantFiltration"]["mem"],
        filter_name = "\"{var}_HardFiltered\"",
        filter_exp = lambda wildcards : "\"QD < 2.0; MQ < 40.0; FS > 60.0; SOR > 3.0; MQRankSum < -12.5; ReadPosRankSum < -8.0\"" if wildcards.var=="SNP" else "\"QD < 2.0; ReadPosRankSum < -20.0; InbreedingCoeff < -0.8; FS > 200.0; SOR > 10.0\""
    shell:
        """
        java -Xmx{params.mem} -jar {config[bin][gatk]} -T VariantFiltration -R {input.ref} -V {input.vcf} --filterName {params.filter_name} --filterExpression {params.filter_exp} -o {output.vcf} 2> {log}
        """
        
rule SelectVariants:
    input : 
        vcf = "results/hard_filtering/common_{var}_tagged.vcf.gz",
        ref = config["reference_genome"]
    output :
        vcf = "results/hard_filtering/VQSR_trainingSet_trusted_3callers_hardFiltered_variants_{var}.vcf.gz",
        tbi = "results/hard_filtering/VQSR_trainingSet_trusted_3callers_hardFiltered_variants_{var}.vcf.gz.tbi"
    log :
        "results/hard_filtering/logs/common_{var}_FilteredSelection.log"
    params:
        mem= config["SelectVariants"]["mem"]
    shell:
        """
        java -Xmx{params.mem} -jar {config[bin][gatk]} -T SelectVariants -R {input.ref} -V {input.vcf} --excludeFiltered -o {output.vcf} 2> {log}
        """
