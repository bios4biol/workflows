# coding: utf-8

__author__ = "Maria BERNARD - Sigenae"
__version__ = "1.0.0"

"""
Index with tabix vcf files
Compute md5 code for final output files
"""

rule md5sum:
    input:
        "{file}"
    
    output:
        "{file}.md5"
        
    shell:
        "{config[bin][md5sum]} {input} > {output}"
        
        
rule tabix:
    input:
        "{file}"
        
    output:
        "{file}.tbi"
        
    shell:
        "{config[bin][tabix]} -p vcf {input} "
        
