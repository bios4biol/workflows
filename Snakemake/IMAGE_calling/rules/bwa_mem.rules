# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria Bernard- Sigenae"
__version__ = "2.0.0"

from snakemake.exceptions import MissingInputException

"""
Rules for mapping reads with bwa mem. Bam files are sorted and indexed with samtools.
"""
            
rule bwa_mem:
    input:
        config["reference_genome"],
        lambda wildcards: all_reads[wildcards.prefix]["reads"]
        
    output:
        temp("results/mapping/{prefix}.rg.sort.bam.bai"),
        bam=temp("results/mapping/{prefix}.rg.sort.bam")
        
    params:
        IDX=lambda wildcards: all_reads[wildcards.prefix]["idx"],
        NAME=lambda wildcards: all_reads[wildcards.prefix]["name"],
        PLATEFORM=lambda wildcards: all_reads[wildcards.prefix]["plateform"],
        min_seed_length="4" if config["min_seed_length"]=="" else config["min_seed_length"],
        
    threads: config["bwa_mem"]["cpu"]
    
    log:
        "results/mapping/logs/{prefix}_mapping.stderr",
        "results/mapping/logs/{prefix}_samtools.stderr"
    
    shell:
        """
        {config[bin][bwa]} mem -M -t {threads} -k {params.min_seed_length} \
        -R '@RG\\tID:{params.IDX}\\tPL:{params.PLATEFORM}\\tSM:{params.NAME}\\tLB:lib_{params.IDX}\\tPU:pu_{params.IDX}\' \
        {input} 2> {log[0]} | {config[bin][samtools]} sort -@ {threads}  -T /tmp/`date +%s`_{wildcards.prefix} -o {output.bam} -O BAM - 2>> {log[1]} ;
        {config[bin][samtools]} index -@ {threads} {output.bam} 2>> {log[1]} 
        """

