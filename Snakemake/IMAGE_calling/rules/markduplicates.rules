# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rule for marking PCR and optical duplicates using Picard MarkDuplicates.
"""

rule markduplicates:
    """
    merged bam files are marked with markduplicates tool
    """
    input:
        "results/sample_merge/{sample}.rg.sort.bam"
        
    output:
        temp("results/markduplicates/{sample}.rg.sort.md.bai"),
        marked_bam=temp("results/markduplicates/{sample}.rg.sort.md.bam"),
        metrics="results/markduplicates/{sample}.rg.sort.md.metrics"
        
    log: 
        stderr="results/markduplicates/logs/{sample}_duplicates.stderr",
        stdout="results/markduplicates/logs/{sample}_duplicates.stdout"
    
    params:
        mem=str(int(config["markduplicates"]["mem"].replace("G","").replace("g",""))-4)+"G"
    
    shell:
        """
        java -Xmx{params.mem} -jar {config[bin][picard]} MarkDuplicates I={input} O={output.marked_bam} M={output.metrics} CREATE_INDEX=true 2> {log.stderr} > {log.stdout}
        """
