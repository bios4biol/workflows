# RNASeq Analysis workflow : quantification and SNP calling

__authors__: Maria Bernard (INRAE - SIGENAE) / Kevin Muret - Frederic Jehl - Sandrine Lagarrigue (AGROCAMPUS OUEST / INRAE - PEGASE)

- [RNASeq Analysis workflow : quantification and SNP calling](#rnaseq-analysis-workflow---quantification-and-snp-calling)
  * [What it does ?](#what-it-does--)
  * [1. Input](#1-input)
  * [2. Data Processing](#2-data-processing)
    + [Raw reads trimming](#raw-reads-trimming)
    + [Sequence alignment](#sequence-alignment)
    + [Transcriptomic Alignment post processing](#transcriptomic-alignment-post-processing)
    + [Gene/Isoform expression](#gene-isoform-expression)
    + [Genomic Alignment post processing](#genomic-alignment-post-processing)
    + [Variant Calling](#variant-calling)
  * [3. Dependencies](#3-dependencies)
  * [4. Configure and running workflow](#4-configure-and-running-workflow)

## What it does ? 

This pipeline will perform classical RNASeq analysis workflow (trimming, STAR mapping, and RSEM expression quantification) but also call variant based on the [GATK RNASeq recommandations](https://software.broadinstitute.org/gatk/documentation/article.php?id=3891).

 The pipeline is represented in the graph below:

![](img/full_dryrun.png)



## 1. Input

The user must use the [config_calling.yaml](config_calling.yaml.example) file to provide all necessary inputs for the pipeline:

- A reference fasta file (fasta_ref option)

- A genome reference GTF file (gtf_ref option)

- A set of known variants used to recalibrate bases quality in GATK preprocessing steps RealignerTargetCreator and BaseRecalibrator (known_vcf option)

- A sample reads description TSV file (sample_config option), with the following columns:

  - idx : a unique index number to identify each fastq (paire) file
  - name : the sample name
  - forward_read and reverse_read are fastq file names. These files need to be in a directory precised with the data_dir option
    - If single end, leave an empty column in the reverse_read
    - If paired, file names need to ends with _R1.fastq.gz or _R2.fastq.gz (or fq instead of fastq, and not necessarly compressed)
  - sequencer : sequencer used to generate the sequences : "ILLUMINA", "SLX", "SOLEXA", "SOLID", "454", "LS454", "COMPLETE", "PACBIO", "IONTORRENT", "CAPILLARY", "HELICOS", "UNKNOWN"
  - oriented : to indicate if sequences are generated with strand-specific protocol. It corresponds to the forward_prob RSEM parameter. Indicate : 
    - 1 for a strand-specific protocol where all (upstream) reads are derived from the forward strand,
    - 0 for a strand-specific protocol where all (upstream) read are derived from the reverse strand,
    - 0.5 for a non-strand-specific protocol. 
  - phred scale: to indicate the phred score scale use to code base quality: either 33 (Sanger and illumina 1.8+) or 64 (Solexa, illumina 1.3 to 1.8 excluded)




## 2. Data Processing

### Raw reads trimming

First step is to filter and trim raw reads before alignment. This step will convert phred 64 scale fastq files into phred 33 scale fastq files and then trim reads with [trim_galore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/). It will trim adapter and filtered out trimmed reads that are smaller than the third of the read length.

You will find in Result/Summary directory, a Log_TrimGalore_summary.tsv that detail the trim_galor statistics.

### Sequence alignment

Trimmed reads will be align against the reference genome thanks to the [STAR](https://academic.oup.com/bioinformatics/article/29/1/15/272537) following the multi-sample 2-pass mapping procedure, with the GTF  reference file.

STAR 2nd pass alignment will generate bam files in genome and in transcriptome coordinates.

You will find in the Results/Summary directory , 2 files reporting alignment statistics : Log_STAR_Aln_1_summary.tsv and Log_STAR_Aln_2_summary.tsv



### Transcriptomic Alignment post processing

Reads with multiple alignment will be filtered out to generate a seconde transcriptomic bam:

```samtools view -q 255```



### Gene/Isoform expression 

Expression will be compute on all reads align on transcriptome or on uniquely mapped reads thanks to [RSEM](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-323). For each sample, it will generate gene and isoforms results.

You will find in the Results/Summary directory, expression quantification for all samples:

* for gene and isorms
* for expected count, TPM and FPKM quantification method
* based on uniquely mapped reads or all alignment.

The file will be named : 

​	RSEM\__[multimap/uniqmap]_\_summary_PREFIX\__[gene/isoforms]_\__[TPM/expected\_count/FPKM]_.tsv

PREFIX is the name of your sample_config file define in the config.yaml file.



### Genomic Alignment post processing

Genomic alignement will be used to call variants. Each sample will have its bam treated like this:

- cleaned thanks to cleanSam from GATK
- annotated with AddOrReplaceReadGroups from [Picard tools](https://broadinstitute.github.io/picard/) based on information you provide in the sample config file
- multimapped reads filtered out (samtools -q 255)
- merged with other bam files belonging to the same sample name (optionnal)
- with PCR and optical duplicates marked thanks to Picard tools
- with splitted alignment convert in separated alignments and mapping quality of 250 reassigned to 60 with SplitNCigarReads from GATK 
- with its indel realign thanks to RealignerTargetCreator and IndelRealigner, and its base quality score recalibrated thanks to BaseRecalibrator and PrintReads from GATK using the known_vcf file provided in the config.yaml.



The final bam alignment files will be find in Results/GATK/ directory and will be called : _[sample]_\_uniq_cs_rg_merge_md_split_real_recal.bam



### Variant Calling

Variants will called thanks to HaplotypeCaller from GATK which will generate one gVCF per sample (Results/GATK/_[sample]_\_.g.vcf.gz files) with the following options:

- -stand_call_conf 20.0 
- --min_base_quality_score 10 
- --min_mapping_quality_score 20

GenotypeGVCFs from GATK will take all gVCF files to generate one VCF file called Results/GATK/PREFIX_full.vcf.gz

Finally, this VCF file will be splitted into two VCF files for SNP or INDEL using SelectVariant from GATK:Results/GATK/PREFIX_SNP.vcf.gz and Results/GATK/PREFIX_INDEL.vcf.gz



## 3. Dependencies

###3.1. Links to binaries

The workflow depends on:  LIST TO CHECK !
* cutadapt
* trimgalore (version 0.4.5)
* STAR (version 2.5.2b)
* picard.jar (version 2.1.1)
* samtools (version 1.3.1 )
* rsem-prepare-reference (RSEM version 1.3.0)
* rsem-calculate-expression (RSEM version 1.3.0)
* GenomeAnalysisTK.jar (version 3.7)
* java (version 8)
* snakemake (version 4.8.0)
* maskFastaFromBed (bedtools version 2.26.0)
* vcf-merge (VCFtools version 0.1.15)
* bgzip (Tabix version 0.2.5)
* tabix (Tabix version 0.2.5)
* python3.6.3 (version 3.6.3) 
* fastqc (version 0.11.7)


Theses softwares need to be available in you PATH or in a bin directory precised in the config.yaml file with the bin_dir option.

###3.2. Using conda mamba

```
conda activate mamba
mamba create -n env_name -c bioconda -c conda-forge cutadapt trim-galore star samtools gatk4 rsem
conda activate /path/to/miniconda/envs/mamba/envs/env_name
module load system/Python-3.6.3
export PERL5LIB=""
```


##  4. Configure and running workflow

- Copy the config_calling.yaml.example file into your working directory and update it according to the instructions inside the file.

- launch snakemake command as presented in SLURM.sh to launch the workflow. This script explain how to launch a dryrun, a full run, perform quantification expression only or variant calling only

- If necessary update resources_SLURM.yaml to define cpu memory and cluster partition resources for each rule. The default section is the minimal resources per job. The other section take as name the rules name and define only resources that are different from the default section.


Resources_SLURM.yaml and SLURM.sh are specific to SLURM cluster. Therefore, if you use another cluster, you need to create other files. However, you can use these files to inspire you.
​    

This is the official documentation of Snakemake if you need more information: <https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html>

