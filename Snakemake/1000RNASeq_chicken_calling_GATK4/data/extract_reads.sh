#!/bin/sh

bam=../Results/STAR_Aln_2/RpRm_adip_F0.ChStress2015.CT.Rp.Fem.32w.2016pe150or.000158.001047a_genomic_multi_merge.bam
fasta=/work/project/1000rnaseqchick/reference/Galgal5_FGF21.fa
gtf=/work/project/1000rnaseqchick/reference/20180731_Ensembl_Rennes_ALDB_NCBI_NONCODE_FR-AgENCODE_final_ordered.gtf

module load bioinfo/samtools-1.8

samtools view $bam | head -n 50000 |cut -f 1 | sort -u | head -n 10000 > read.list
wc -l read.list 
# 10000 read.list

samtools view $bam | head -n 50000 |cut -f 3 | sort -u 
# 1

# extract reads from fastq
# R1
grep RpRm_adip_F0.ChStress2015.CT.Rp.Fem.32w.2016pe150or.000158.001047a ../RpRm_adip_F0.tsv | cut -f 3
# Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R1.fastq.gz

# R2
grep RpRm_adip_F0.ChStress2015.CT.Rp.Fem.32w.2016pe150or.000158.001047a ../RpRm_adip_F0.tsv | cut -f 4
# Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R2.fastq.gz

DATA_DIR=/work/project/1000rnaseqchick/internal_runs/data/Project_ChickStress.528/Run_Plaque3_Pool5.10058/RawData/
for i in `ls $DATA_DIR/TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005*`
do
 

done

bioawk -c fastx -v out=Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R1.fastq 'BEGIN{while(getline<"read.list">0){tab[$1]=1}; c=0}{
     if(tab[$name]==1 && c<10000){
         print "@"$name"/1\n"$seq"\n+\n"$qual >> out
         c++
     }
     if ( c == 10000){exit}
     if (c%500 == 0) {print c}
 }' $DATA_DIR/TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R1.fastq.gz
gzip Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R1.fastq

bioawk -c fastx -v out=Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R2.fastq 'BEGIN{while(getline<"read.list">0){tab[$1]=1}}{
     if(tab[$name]==1 && c<10000){
         print "@"$name"/1\n"$seq"\n+\n"$qual >> out
         c++
     }
     if ( c == 10000){exit}
     if (c%500 == 0) {print c}
 }' $DATA_DIR/TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R2.fastq.gz
gzip Run_Plaque3_Pool5.10058_TA-158-1047-Rp-CT_ATTCAGAA-TCAGAGCC_L005_R2.fastq

# extract chr1 from ref
getseq -f $fasta -name 1 -o Chr1_`basename $fasta`
awk '$1==1' $gtf >> Chr1_`basename $gtf`

# extract sample_data_conf
grep -E 'idx|RpRm_adip_F0.ChStress2015.CT.Rp.Fem.32w.2016pe150or.000158.001047a' ../RpRm_adip_F0.tsv
