import yaml
import pandas as pd
import gzip
import glob
import os,sys
import re

#################################################################################################
# AJOUTER FASTQC
#         BAMQC
#   MULTIQC
#################################################################################################
#### CHECK PARAM AND SAMPLE CONFIG FILE, SET GLOBAL VARIABLE

# params
if not "sample_config" in config or not os.path.exists(config["sample_config"]):
    raise Exception("You need to provide an existing sample config tabular file, using sample_config in your config.yaml file")

if not "data_dir" in config or not os.path.exists(config["data_dir"]):
    raise Exception("You need to provide an existing data_dir directory, using data_dir in your config.yaml file")

if not "fasta_ref" in config or not os.path.exists(config["fasta_ref"]):
    raise Exception("You need to provide an existing fasta reference file, using fasta_ref in your config.yaml file")

if not "gtf_ref" in config or not os.path.exists(config["gtf_ref"]):
    raise Exception("You need to provide an existing gtf reference file, using gtf_ref in your config.yaml file")

if not "known_vcf" in config or not os.path.exists(config["known_vcf"]):
    raise Exception("You need to provide an existing reference VCF file, using known_vcf in your config.yaml file")
    
if "bin_dir" in config:
    os.environ['PATH'] = config["bin_dir"] + os.pathsep + os.environ['PATH']

# sample config
table = pd.read_csv(config["sample_config"], dtype=str,sep='\t')
print(table)
available_PL=["ILLUMINA","SLX","SOLEXA","SOLID","454","LS454","COMPLETE","PACBIO","IONTORRENT","CAPILLARY","HELICOS","UNKNOWN"]
sequencer=table.sequencer.unique()
print(sequencer)
for i in sequencer:
    if not i.upper() in available_PL:
        raise Exception(i.upper()+" sequencer type povided in your sample_config file is not allowed, please choose among : " + ",".join(available_PL))

if table.shape[0] != len(table["forward_read"].unique()):
    raise Exception("Some forward_read files is not unique in your sample config file\n")
    
# global variables
STAT=["expected_count", "TPM", "FPKM"]
POP=os.path.splitext(os.path.basename(config["sample_config"]))[0]
print("POP",POP)
STAR_INDEX_FILE=['genomeParameters.txt', 'chrName.txt', 'chrLength.txt', 'chrStart.txt', 'chrNameLength.txt', 'exonGeTrInfo.tab', 'geneInfo.tab', 'transcriptInfo.tab', 'exonInfo.tab', 'sjdbList.fromGTF.out.tab', 'sjdbInfo.txt', 'sjdbList.out.tab', 'Genome', 'SA', 'SAindex']
RSEM_SUFFIX=["grp", "ti","chrlist","transcripts.fa", "seq","idx.fa","n2g.idx.fa"]
#################################################################################################
## CHECK RESOURCES

with open(config["resources"]) as yml:
    config.update(yaml.load(yml, Loader=yaml.SafeLoader))

if not "__default__" in config or not "cpu" in config["__default__"] or not "mem" in config["__default__"] :
    raise Exception("resources config file need to define at least default resources cpu mem (minimum 5G) in a __default__ section")
if int(config["__default__"]["mem"].replace("G","")) < 5 :
    raise Exception("you need at least 5G of memory per job to run this pipeline")

def check_param(rule,resource):
    if not rule in config:
        config[rule]={resource:config["__default__"][resource]}
    elif not resource in config[rule]:
        config[rule][resource] = config["__default__"][resource]
        
rule_resources = {
    "star_index_pass1" : "cpu",
    "star_aln_pass1":"cpu",
    "star_index_pass2":"cpu",
    "star_aln_pass2":"cpu",
    "merge_bam_star2" : "cpu",
    "clean_SAM" :"mem",
    "add_RG" :"mem",
    "merge_bam" : "cpu",
    "fai_dict_index" : "mem",
    "rsem_index" : "cpu",
    "rsem_count" : "cpu",
    "markdup" : "mem",
    "SplitNCigarReads" :"mem",
    "RealignerTargetCreator" :"mem",
    "IndelRealigner" : "mem",
    "BaseRecalibrator" : "mem",
    "PrintReads" : "mem",
    "individual_gVCF_HaplotypeCaller" : "mem",
    "genotypeGVCF" : "mem",
    "selectVariant" : "mem"
}

for rule in rule_resources:
    check_param(rule,rule_resources[rule])

#################################################################################################
## FUNCTIONS
   
#def find_jar(jar):
#    path=""
#    for bin_dir in os.environ['PATH'].split(os.pathsep):
#        if os.path.exists(os.path.join(bin_dir,jar)):
#           path = os.path.join(bin_dir,jar)
#    if path != "":
#        return path
#    else:
#        raise Exception("Could not find "+jar+" path in PATH")


def gatk_multi_arg(flag, files):
    flag += " "
    return " ".join(flag + f for f in files)

#################################################################################################
## TODO
# ADD rule for merging flagstat
# ADD rule for merging individual HC stat log
# ADD split SNP/INDEL
# ADD VCF filter

## RULES
rule all:
    input: 
        # TrimGalore merge log
        "Results/Summary/Log_TrimGalore_summary.tsv",
        # STAR merge
        "Results/Summary/Log_STAR_Aln_1_summary.tsv", "Results/Summary/Log_STAR_Aln_2_summary.tsv",
        expand("Results/STAR_Aln_2/{sample}_genomic_multi_merge.bam", sample=table.sample_name.unique()),
        # rsem
        expand("Results/Summary/RSEM_multimap_summary_"+POP+"_{feature}_{stat}.tsv", feature=['genes','isoforms'], stat=STAT),
        expand("Results/Summary/RSEM_uniqmap_summary_"+POP+"_{feature}_{stat}.tsv", feature=['genes','isoforms'], stat=STAT),
        #~ # gatk
        expand("Results/GATK/{sample}.g.vcf.gz", sample=table.sample_name.unique()),
        #~ "Results/GATK/all.vcf"
        "Results/GATK/" + POP + "_full.vcf.gz", 
        "Results/GATK/" + POP + "_SNP.vcf.gz", 
        "Results/GATK/" + POP + "_INDEL.vcf.gz"

def get_phred64(wildcards):
    for f in table["forward_read"].tolist() + table["reverse_read"].dropna().tolist() :
        if re.search('^' + wildcards.prefix + '\.f.{0,3}q(\.gz){0,1}$',f):
            return config["data_dir"] + "/" + f
            
rule convertPhred:
    input:
        fastq_in = get_phred64
    output:
        fastq_out = "Results/Phred_convert/{prefix}.fastq.gz"
    script:
        "script/convertPhred64_to_Phred33.py"


def get_fastq(wildcards):
    inputs = list()
    for f in table["forward_read"] :
        if re.search('^' + wildcards.prefix + '(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$',f):
            is_phred64 = table[table["forward_read"] == f]["phred_scale"] == '64'
            if is_phred64.all():
                inputs.append("Results/Phred_convert/" + f)
            else:
                inputs.append(config["data_dir"] + "/" + f)
            r = table[table["forward_read"] == f]["reverse_read"]
            if not r.isnull().values.any():
                if is_phred64.all():
                    inputs.append("Results/Phred_convert/" + r.squeeze())
                else:
                    inputs.append(config["data_dir"] + "/" + r.squeeze())
    print("INPUTS : ",inputs)
    return inputs


rule trim_se:
    input:
        get_fastq
    output:
        out=temp("Results/TrimGalore/{prefix}_trim.fastq.gz")
    params:
        min_trim=lambda wildcards : str(int([ int(table[table["forward_read"] == f]["read_length"].tolist()[0]) for f in table["forward_read"] if f.startswith(wildcards.prefix)][0] / 3 )),
        qual=config["trimming_quality"]
    log:
        "Results/TrimGalore/{prefix}_trimSe.log"
    shell:
        """
        trim_galore --no_report_file --length {params.min_trim} --quality {params.qual} -o `dirname {output.out}` {input} 2> {log}
        mv `dirname {output.out}`/{wildcards.prefix}*trimmed.fq.gz {output.out}
        """

rule trim_pe:
    input:
        get_fastq
    output:
        out1="Results/TrimGalore/{prefix}_trim_R1.fastq.gz",
        out2="Results/TrimGalore/{prefix}_trim_R2.fastq.gz"
    log:
        "Results/TrimGalore/{prefix}_trimPe.log"
    params:
        min_trim=lambda wildcards : str(int([ int(table[table["forward_read"] == f]["read_length"].tolist()[0]) for f in table["forward_read"] if f.startswith(wildcards.prefix)][0] / 3 )),
        qual=config["trimming_quality"]
    shell:
        """
        trim_galore --paired --no_report_file --length {params.min_trim} --quality {params.qual} -o `dirname {output.out1}` {input} 2> {log}
        mv `dirname {output.out1}`/{wildcards.prefix}*_val_1.fq.gz {output.out1}
        mv `dirname {output.out1}`/{wildcards.prefix}*_val_2.fq.gz {output.out2}
        """
        
#~ min_trim=lambda wildcards : str(table[table.forward_read.str.contains('^' + wildcards.prefix)]["read_length"].values[0] / 3)


def get_trim_Log(wildcards):
    inputs=list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        r = table[table["forward_read"] == f]["reverse_read"]
        if not r.isnull().values.any():
            inputs.append("Results/TrimGalore/" + prefix + "_trimPe.log" )
        else:
            inputs.append("Results/TrimGalore/" + prefix + "_trimSe.log" )
    return inputs
    
rule log_merge_trim:
    input:
        logs=get_trim_Log
    output:
        out="Results/Summary/Log_TrimGalore_summary.tsv"
    script:
        "script/trimgalore_summary.py"
########################################################################
# STAR
rule star_index_pass1:
    input:
        fasta=os.path.abspath(config["fasta_ref"]),
        gtf=os.path.abspath(config["gtf_ref"])
    output:
        expand('Results/STAR_Index1/{file}', file=STAR_INDEX_FILE)
    threads: config["star_index_pass1"]["cpu"]
    params:
        sjdbOverhang=max([int(x) for x in table.read_length]) - 1
    shell:
        "STAR --runThreadN {threads} --runMode genomeGenerate --genomeDir Results/STAR_Index1  --genomeFastaFiles {input.fasta} --sjdbOverhang {params.sjdbOverhang} --limitSjdbInsertNsj 2050000  --sjdbGTFfile {input.gtf} "

def get_fastq_trim(wildcards):
    inputs = list()
    for f in table["forward_read"] :
        if re.search('^' + wildcards.prefix + '(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$',f):
            inputs.append("Results/TrimGalore/" + f.replace(wildcards.prefix,wildcards.prefix + "_trim"))
            r = table[table["forward_read"] == f]["reverse_read"]
            if not r.isnull().values.any():
                inputs.append("Results/TrimGalore/" + r.squeeze().replace(wildcards.prefix,wildcards.prefix + "_trim"))
    return inputs
    
rule star_aln_pass1:
    input:
        expand('Results/STAR_Index1/{file}', file=STAR_INDEX_FILE[1:]),
        index1='Results/STAR_Index1/genomeParameters.txt',
        fastq = get_fastq_trim
    output:
        temp("Results/STAR_Aln_1/{prefix}_Log.out"), 
        temp("Results/STAR_Aln_1/{prefix}_Log.progress.out"),
        bam=temp("Results/STAR_Aln_1/{prefix}_Aligned.sortedByCoord.out.bam"),
        sj_out_tab="Results/STAR_Aln_1/{prefix}_SJ.out.tab"
    log:
        "Results/STAR_Aln_1/{prefix}_Log.final.out"
    threads: config["star_aln_pass1"]["cpu"]
    shell:
        "STAR --runThreadN {threads} --genomeDir `dirname {input.index1}` --readFilesIn {input.fastq} --readFilesCommand zcat --outFileNamePrefix `dirname {output.bam}`/{wildcards.prefix}_ --outSAMtype BAM SortedByCoordinate --outFilterType BySJout"

def get_SJout_STAR1(wildcards):
    inputs=list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results/STAR_Aln_1/" + prefix + "_SJ.out.tab" )
    return inputs

rule star_index_pass2:
    input:
        fasta=os.path.abspath(config["fasta_ref"]),
        gtf=os.path.abspath(config["gtf_ref"]),
        sj_out_tab=get_SJout_STAR1
    output:
        expand('Results/STAR_Index2/{file}', file=STAR_INDEX_FILE)
    threads: config["star_index_pass2"]["cpu"]
    params:
        sjdbOverhang=max([int(x) for x in table.read_length]) - 1
    shell:
        " STAR --runThreadN {threads} --runMode genomeGenerate --genomeDir Results/STAR_Index2 --genomeFastaFiles {input.fasta} --sjdbOverhang {params.sjdbOverhang} --limitSjdbInsertNsj 2050000 --sjdbGTFfile {input.gtf} --sjdbFileChrStartEnd {input.sj_out_tab}"

rule star_aln_pass2:
    input:
        expand('Results/STAR_Index2/{file}', file=STAR_INDEX_FILE[1:]),
        index2='Results/STAR_Index2/genomeParameters.txt',
        fastq = get_fastq_trim
    output:
        temp("Results/STAR_Aln_2/{prefix}_Log.out"),
        temp("Results/STAR_Aln_2/{prefix}_Log.progress.out"),
        temp("Results/STAR_Aln_2/{prefix}_SJ.out.tab"),
        g_bam="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.bam",
        t_bam="Results/STAR_Aln_2/{prefix}_Aligned.toTranscriptome.out.bam",
        log=temp("Results/STAR_Aln_2/{prefix}_Log.final.out"),
        g_stat="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.flagstat"
    threads: config["star_aln_pass2"]["cpu"]
    priority : 50
    shell:
        """
        STAR --runThreadN {threads} --genomeDir `dirname {input.index2}` --readFilesIn {input.fastq} --readFilesCommand zcat --outFileNamePrefix `dirname {output.g_bam}`/{wildcards.prefix}_ --outSAMtype BAM SortedByCoordinate --quantMode TranscriptomeSAM 
        samtools flagstat {output.g_bam} > {output.g_stat}
        """
        
def get_STAR_log(wildcards):
    inputs = list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results/" + wildcards.dir + "/" + prefix + "_Log.final.out" )
    return inputs
    
rule log_merge_STAR:
    input:
        logs=get_STAR_log
    output:
        out="Results/Summary/Log_{dir}_summary.tsv"
    wildcard_constraints:
        dir='STAR_Aln_[12]'
    script:
        "script/star_summary.py"

rule clean_SAM:
    input :
        g_bam="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.bam",
    output : 
        g_bam=temp("Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_cs.bam"),
        g_stat="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_cs.flagstat"
    params :
        mem=str(int(config["clean_SAM"]["mem"].replace("G","")) -4 )+"G" if int(config["clean_SAM"]["mem"].replace("G","")) -4 > 1 else config["clean_SAM"]["mem"]
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" CleanSam I={input.g_bam} O={output.g_bam}
        samtools flagstat {output.g_bam} > {output.g_stat}
        """

rule add_RG:
    input :
        g_bam="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_cs.bam",
    output : 
        g_bam=temp("Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_cs_rg.bam"),
    params :
        idx  = lambda wildcards : [ table[table["forward_read"] == f]["idx"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        name = lambda wildcards : [ table[table["forward_read"] == f]["sample_name"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        sequencer = lambda wildcards : [ table[table["forward_read"] == f]["sequencer"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        mem=str(int(config["add_RG"]["mem"].replace("G","")) -4 )+"G" if int(config["add_RG"]["mem"].replace("G","")) -4 > 1 else config["add_RG"]["mem"] ,
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" AddOrReplaceReadGroups I={input.g_bam} O={output.g_bam} RGID={params.idx} RGLB={params.name} RGPL={params.sequencer} RGPU="-" RGSM={params.name} 
        """

def get_STAR2_genomic_bam(wildcards):
    inputs = list()
    for f in table[table["sample_name"] == wildcards.sample]["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results/STAR_Aln_2/" + prefix + "_Aligned.sortedByCoord_cs_rg.bam")
    return inputs
                
rule merge_bam_star2:
    input:
        g_bam=get_STAR2_genomic_bam
    output:
        g_bam="Results/STAR_Aln_2/{sample}_genomic_multi_merge.bam",
        g_stat="Results/STAR_Aln_2/{sample}_genomic_multi_merge.flagstat"
    threads:  config["merge_bam_star2"]["cpu"]
    shell:
        """
        a=`echo {input.g_bam} | wc -w `
        if [[ $a > 1 ]]
        then 
        samtools merge -@{threads} {output.g_bam} {input.g_bam}
        else
        echo 
        cp {input.g_bam} {output.g_bam}
        fi
        samtools flagstat {output.g_bam} > {output.g_stat}
        """

# /STAR
########################################################################

rule remove_multimap:
    input:
        g_bam="Results/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_cs_rg.bam",
        t_bam="Results/STAR_Aln_2/{prefix}_Aligned.toTranscriptome.out.bam"
    output:
        g_bam=temp("Results/Bam_preprocess/{prefix}_cs_rg_genomic_uniq.bam"),
        t_bam=temp("Results/Bam_preprocess/{prefix}_transcriptomic_uniq.bam"),
        g_stat="Results/Bam_preprocess/{prefix}_cs_rg_genomic_uniq.flagstat",
    shell:
        """
        samtools view -h -q 255 {input.g_bam} -bo {output.g_bam} 
        samtools view -h -q 255 {input.t_bam} -bo {output.t_bam} 
        samtools flagstat {output.g_bam} > {output.g_stat}
        """
########################################################################
# RSEM
rule rsem_index:
    input:
        fasta=os.path.abspath(config["fasta_ref"]),
        gtf=os.path.abspath(config["gtf_ref"])
    output:
        rsem=temp(expand("Results/Rsem_Index/" + os.path.splitext(os.path.basename(config["fasta_ref"]))[0]+".{suffix}", suffix=RSEM_SUFFIX))
    params:
        rsem_prefix=os.path.splitext(os.path.basename(config["fasta_ref"]))[0]
    threads: config["rsem_index"]["cpu"]
    shell:
        """
        ln -s {input} Results/Rsem_Index/.
        rsem-prepare-reference -p {threads} --gtf {input.gtf} {input.fasta} Results/Rsem_Index/{params.rsem_prefix}
        """

def get_RSEM_inbam(wildcards):
    for f in table["forward_read"] :
        if re.search('^' + wildcards.prefix + '(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$',f):
            if wildcards.dir == "RSEM_multimap":
                return "Results/STAR_Aln_2/" + wildcards.prefix + "_Aligned.toTranscriptome.out.bam"
            else:
                return "Results/Bam_preprocess/" + wildcards.prefix + "_transcriptomic_uniq.bam"

rule rsem_count:
    input:
        expand("Results/Rsem_Index/" + os.path.splitext(os.path.basename(config["fasta_ref"]))[0]+".{suffix}", suffix=RSEM_SUFFIX[1:]),
        rsem_idx="Results/Rsem_Index/" + os.path.splitext(os.path.basename(config["fasta_ref"]))[0]+".grp",
        bam=get_RSEM_inbam
    output:
        "Results/{dir}/{prefix}.genes.results",
        "Results/{dir}/{prefix}.isoforms.results"
    params:
        forward_prob = lambda wildcards : table[table.forward_read.str.contains('^' + wildcards.prefix)]["oriented"].tolist()[0],
        mode = lambda wildcards : "--paired-end" if table[table.forward_read.str.contains('^' + wildcards.prefix)]["reverse_read"].any() else "" ,
        prefixOut="Results/{dir}/{prefix}"
    threads: config["rsem_count"]["cpu"]
    shell:
        "rsem-calculate-expression --ci-memory 30000 {params.mode} --bam --no-bam-output --estimate-rspd --calc-ci --seed 12345 -p {threads} --forward-prob {params.forward_prob} {input.bam} `echo {input.rsem_idx} | sed 's/.grp//'` {params.prefixOut}"

def get_RSEM_res(wildcards):
    inputs=list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results/" + wildcards.dir + "/" + prefix + "."+wildcards.feature+".results" )
    return inputs
    
rule merge_rsem:
    input:
        rcount=get_RSEM_res
    output:
        out=expand("Results/Summary/{{dir}}_summary_"+POP+"_{{feature}}_{stat}.tsv", stat=STAT)
    params:
        stat=STAT,
        sample_names=table["sample_name"].tolist() 
    script:
        "script/rsem_summary.py"
# /RSEM
########################################################################

def get_genomic_bam(wildcards):
    inputs = list()
    for f in table[table["sample_name"] == wildcards.sample]["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results/Bam_preprocess/" + prefix + "_cs_rg_genomic_uniq.bam" )
    return inputs
    
rule merge_bam : 
    input:
        g_bam=get_genomic_bam
    output:
        g_bam=temp("Results/Bam_preprocess/{sample}_cs_rg_genomic_uniq_merge.bam"),
        g_stat="Results/Bam_preprocess/{sample}_cs_rg_genomic_uniq_merge.flagstat"
    threads:  config["merge_bam"]["cpu"]
    shell:
        """
        a=`echo {input.g_bam} | wc -w `
        if [[ $a > 1 ]]
        then 
        samtools merge -@{threads} {output.g_bam} {input.g_bam}
        else
        echo 
        cp {input.g_bam} {output.g_bam}
        fi
        samtools flagstat {output.g_bam} > {output.g_stat}
        """

# FAI, DICT, RSEM
rule fai_dict_index:
    input:
        fasta=os.path.abspath(config["fasta_ref"]),
        gtf=os.path.abspath(config["gtf_ref"])
    output:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        fai="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"])+".fai",
        dict="Results/Fai_Dict_Index/" + os.path.splitext(os.path.basename(config["fasta_ref"]))[0]+".dict"
    params:
        rsem_prefix=os.path.splitext(os.path.basename(config["fasta_ref"]))[0],
        mem=config["fai_dict_index"]["mem"]
    shell:
        """
        ln -s {input.fasta} {output.fasta}
        samtools faidx {output.fasta}
        gatk --java-options "-Xmx{params.mem}" CreateSequenceDictionary R={output.fasta} O={output.dict}
        """

rule markdup:
    input:
        "Results/Bam_preprocess/{sample}_cs_rg_genomic_uniq_merge.bam"
    output:
        temp("Results/Bam_preprocess/{sample}_uniq_cs_rg_merge_md.bai"),
        bam=temp("Results/Bam_preprocess/{sample}_uniq_cs_rg_merge_md.bam"),
        metrics="Results/Bam_preprocess/{sample}_md.metrics.txt",
        stat="Results/Bam_preprocess/{sample}_uniq_cs_rg_merge_md.flagstat"
    params:
        mem=str(int(config["markdup"]["mem"].replace("G","")) -4 )+"G" if int(config["markdup"]["mem"].replace("G","")) -4 > 1 else config["markdup"]["mem"] ,
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" MarkDuplicates I={input} O={output.bam} CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT M={output.metrics}
        samtools flagstat {output.bam} > {output.stat}
        """

######
# GATK

rule SplitNCigarReads:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        bam="Results/Bam_preprocess/{sample}_uniq_cs_rg_merge_md.bam",
        bai="Results/Bam_preprocess/{sample}_uniq_cs_rg_merge_md.bai"
    output:
        "Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
        stat="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.flagstat"
    params:
        mem=config["SplitNCigarReads"]["mem"]
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" SplitNCigarReads -R {input.fasta} -I {input.bam} -O {output.bam} 
        samtools flagstat {output.bam} > {output.stat}
        """

#rule RealignerTargetCreator:
#    input:
#        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
#        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
#        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
#        known_vcf=config["known_vcf"]
#    output:
#        temp("Results/GATK/{sample}.real.intervals")
#    params:
#        mem=config["RealignerTargetCreator"]["mem"]
#    shell:
#        """
#        gatk --java-options "-Xmx{params.mem}" RealignerTargetCreator -R {input.fasta} -o {output} -known {input.known_vcf} -I {input.bam}
#        """

#rule IndelRealigner:
#    input:
#        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
#        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
#        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
#        intervals="Results/GATK/{sample}.real.intervals"
#    output:
#        temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_real.bai"),
#        bam=temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_real.bam"),
#        stat="Results/GATK/{sample}_uniq_cs_rg_merge_md_split_real.flagstat"
#    params:
#        mem=config["IndelRealigner"]["mem"],
#    shell:
#        """
#        gatk --java-options "-Xmx{params.mem}" IndelRealigner -I {input.bam}  -R {input.fasta} -targetIntervals {input.intervals} -o {output.bam}
#        samtools flagstat {output.bam} > {output.stat}
#        """

rule BaseRecalibrator:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
        known_vcf=config["known_vcf"]
    output:
        "Results/GATK/{sample}.recal.table"
    params:
        mem=config["BaseRecalibrator"]["mem"],
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" BaseRecalibrator --QUIET true -I {input.bam} -R {input.fasta} --known-sites {input.known_vcf} -O {output} 
        """

#rule PrintReads:
#    input:
#        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
#        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
#        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
#        intervals="Results/GATK/{sample}.recal.intervals"
#    output:
#        bam=temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bam"),
#        bai=temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bai"),
#        stat="Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.flagstat"
#    params:
#        mem=config["PrintReads"]["mem"],
#    shell:
#        """
#        gatk --java-options "-Xmx{params.mem}" PrintReads -I {input.bam} -R {input.fasta} -BQSR {input.intervals} -O {output.bam} 
#        samtools flagstat {output.bam} > {output.stat}
#        """
 
rule ApplyBQSR:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bam",
        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split.bai",
        table="Results/GATK/{sample}.recal.table"
    output:
        bam=temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bam"),
        bai=temp("Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bai"),
        stat="Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.flagstat"
    params:
        mem=config["PrintReads"]["mem"],
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" ApplyBQSR -I {input.bam} -R {input.fasta} --bqsr-recal-file {input.table} -O {output.bam} 
        samtools flagstat {output.bam} > {output.stat}
        """        

rule individual_gVCF_HaplotypeCaller:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        bam="Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bam",
        bai="Results/GATK/{sample}_uniq_cs_rg_merge_md_split_recal.bai"
    output:
        gvcf="Results/GATK/{sample}.g.vcf.gz",
        log="Results/GATK/{sample}_individual_HC.log"
    params:
        mem=config["individual_gVCF_HaplotypeCaller"]["mem"],
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" HaplotypeCaller -I {input.bam} -R {input.fasta} --min-base-quality-score 10 --minimum-mapping-quality 20 -ERC GVCF --dont-use-soft-clipped-bases -O {output.gvcf} 2> {output.log}
        """

rule combineGVCF:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        gvcf=expand("Results/GATK/{sample}.g.vcf.gz", sample=table.sample_name.unique())
    output:
        gvcf="Results/GATK/"+POP+".g.vcf.gz",
        log="Results/GATK/"+POP+".g.vcf.log"
    params:
        mem=config["combineGVCF"]["mem"],
        gvcfs=lambda wildcards, input: list(map("--variant {}".format, input.gvcf))
    shell:
        """
        gatk --java-options \"-Xmx{params.mem}\" CombineGVCFs {params.gvcfs} -R {input.fasta} -O {output.gvcf} 2> {output.log}
        """

rule genotypeGVCF:
    input:
        fasta="Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        gvcf="Results/GATK/"+POP+".g.vcf.gz"
    output:
        vcf="Results/GATK/"+POP+"_full.vcf.gz",
        log="Results/GATK/"+POP+"_full.log"
    params:
        mem=config["genotypeGVCF"]["mem"],
    shell:
        """
        gatk --java-options \"-Xmx{params.mem}\" GenotypeGVCFs -V {input.gvcf} -R {input.fasta} -stand-call-conf 20 -O {output.vcf} 2> {output.log}
        """

# /GATK        
#######

rule selectVariant:
    input:
        ref = "Results/Fai_Dict_Index/" + os.path.basename(config["fasta_ref"]),
        vcf = "Results/GATK/"+POP+"_full.vcf.gz"
    output:
        snp = "Results/GATK/"+POP+"_SNP.vcf.gz",
        indel = "Results/GATK/"+POP+"_INDEL.vcf.gz"
    params:
        mem=config["selectVariant"]["mem"],
    shell:
        """
        gatk --java-options \"-Xmx{params.mem}\" SelectVariants -R {input.ref} -V {input.vcf} -select-type SNP -O {output.snp}
        gatk --java-options \"-Xmx{params.mem}\" SelectVariants -R {input.ref} -V {input.vcf} -select-type INDEL -O {output.indel}
        """

# ADD rule Hard filtering
# ADD rule filtering on junction
